import UIKit
import SnapKit
import Lottie

class ViewController: UIViewController {
    
    lazy var logo: AnimationView = {
        //
        // Ovdje mijenjas ime filea                vv
        //
        let path = Bundle.main.path(forResource: "test", ofType: "json")
        
        let view = AnimationView(filePath: path!)
        
        return view
    }()
                
    func addSubviews() {
        view.addSubview(logo)
    }
    
    func layout() {
        logo.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor =
            UIColor(red: 245.0 / 255.0, green: 243.0 / 255.0, blue: 235.0 / 255.0, alpha: 1.0)
        
        addSubviews()
        layout()
        
        logo.play()
    }


}

